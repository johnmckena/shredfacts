use chrono::{Datelike, Timelike, Utc};
use time::Duration;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    dataprocessor::load_revenue().await;
    dataprocessor::load_income().await;

    Ok(())
}

pub mod dataprocessor {
    use std::ops::Sub;

    use chrono::{Datelike, NaiveDate};
    use mysql::{params, prelude::Queryable, Pool};

    #[derive(Debug, PartialEq, Eq)]
    struct Revenue {
        cik: Option<String>,
        key: Option<String>,
        from_date: Option<String>,
        to_date: Option<String>,
        value: Option<String>,
        decimals: Option<String>,
        unitref: Option<String>,
        context: Option<String>,
        year: Option<String>,
        days: Option<String>,
        is_annual: Option<bool>,
        is_annual_revenue: Option<bool>,
    }

    #[derive(Debug, PartialEq, Eq)]
    struct Fact {
        cik: Option<String>,
        id: Option<String>,
        keytext: Option<String>,
        valuetext: Option<String>,
        context: Option<String>,
        decimals: Option<String>,
        unitref: Option<String>,
    }

    #[derive(Debug, PartialEq, Eq)]
    struct DateInfo {
        from_date: Option<String>,
        to_date: Option<String>,
        year: Option<String>,
        quarter: Option<String>,
        is_annual: Option<bool>,
        days: Option<String>,
        is_annual_revenue: Option<bool>,
    }

    #[derive(Debug, PartialEq, Eq)]
    struct ContextDateLocation {
        token_index: usize,
        year_token: String,
    }

    async fn select_revenue() -> Vec<Fact> {
        let url = "mysql://db_user:db_user_pass@localhost:6033/app_db";
        let pool = Pool::new(url).unwrap();
        let mut conn = pool.get_conn().unwrap();
        // Let's select payments from database. Type inference should do the trick here.
        let facts = conn.query_map(
            "SELECT cik, id, keytext, valuetext, context, decimals, unitref from findata WHERE keytext like '%revenue%'",
            |(cik, id, keytext, valuetext, context, decimals, unitref)| Fact {
                cik,
                id,
                keytext,
                valuetext,
                context,
                decimals,
                unitref
            },
        ).unwrap();

        facts
    }

    async fn get_date_info(context: Option<String>) -> DateInfo {
        // extract year and quarter
        let context_string = context.unwrap();
        let contextparts = context_string.split("_");
        let context_part_count = contextparts.clone().count();
        let context_parts_vec: Vec<&str> = contextparts.collect();
        let mut context_parts_date_locations: Vec<ContextDateLocation> = vec![];
        let mut from_date_string = "";
        let mut to_date_string = "";
        let mut is_annual_revenue = false;
        let mut context_date_format = "";
        let mut days = 0;
        let mut calendarYear = 0;
        let mut is_annual = false;
        let mut quarter = "default";
        let mut from_date: NaiveDate = NaiveDate::from_ymd(1900, 01, 01);
        let mut to_date: NaiveDate = NaiveDate::from_ymd(1900, 01, 01);

        let mut count = 0;

        for context_part in context_parts_vec.iter() {

            if context_part.contains("2017") {
                let context_date_location_2017 = ContextDateLocation {
                    token_index: count,
                    year_token: "2017".to_string(),
                };

                context_parts_date_locations.push(context_date_location_2017);
            }

            if context_part.contains("2018") {
                let context_date_location_2018 = ContextDateLocation {
                    token_index: count,
                    year_token: "2018".to_string(),
                };

                context_parts_date_locations.push(context_date_location_2018);
            }

            if context_part.contains("2019") {
                let context_date_location_2019 = ContextDateLocation {
                    token_index: count,
                    year_token: "2019".to_string(),
                };

                context_parts_date_locations.push(context_date_location_2019);
            }

            if context_part.contains("2020") {
                let context_date_location_2022 = ContextDateLocation {
                    token_index: count,
                    year_token: "2020".to_string(),
                };

                context_parts_date_locations.push(context_date_location_2022);
            }

            if context_part.contains("2021") {
                let context_date_location_2021 = ContextDateLocation {
                    token_index: count,
                    year_token: "2021".to_string(),
                };

                context_parts_date_locations.push(context_date_location_2021);
            }

            count = count + 1;
        }

        if context_string.starts_with("FD") {
            println!("FD format");
            context_date_format = "FD";
        } else {
            context_date_format = "C_";
        }

        match context_date_format.as_ref() {
            "FD" => {
                //println!("FD context date format");

                //FD2017Q4YTD
                //FI2019Q4

                let calendaryear_string = &context_parts_vec[0][2..6];
                calendarYear = calendaryear_string.parse::<i32>().unwrap();

                quarter = context_parts_vec[0][6..8].as_ref();

                let ending: &str = context_parts_vec[0][8..11].as_ref();

                if context_parts_vec.len() == 1 && context_parts_vec[0].contains("Q4") {
                    //println!("ending   {}", ending);
                    is_annual_revenue = true;
                } else {
                    is_annual_revenue = false;
                }

                println!("{}", context_parts_vec[0]);
                println!("{}", calendaryear_string);
                println!("{}", quarter);
                println!("{}", ending);

                match ending {
                    "YTD" => is_annual = true,
                    "QTD" => is_annual = false,
                    _ => println!("missing context ending  info"),
                }
            }
            // "C_" => {
            //     println!("C_ context date format");

            //     if context_part_count > 5 {
            //         from_date_string = context_parts_vec[4];
            //         to_date_string = context_parts_vec[5];
            //     }

            //     if context_part_count == 4 {
            //         from_date_string = context_parts_vec[2];
            //         to_date_string = context_parts_vec[3];
            //         is_annual_revenue = true;
            //     }
            // }
            _ => {
                print!("Unknow context date format");
                if context_parts_date_locations.len() > 0 {
                    let year_token = context_parts_date_locations[0].year_token.clone();
                    calendarYear = year_token.parse::<i32>().unwrap();
                    let datetext = context_parts_vec[context_parts_date_locations[0].token_index];
                    let i = datetext.find(&year_token).unwrap();

                    if datetext.len() >= (i + 8) {
                        from_date_string = &datetext[i..i + 8];
                    } else if datetext.len() >= (i + 6) {
                        from_date_string = &datetext[i..i + 6];
                    }

                }

                if context_parts_date_locations.len() > 1 {
                    //look for second date
                    let year_token2 = context_parts_date_locations[1].year_token.clone();
                    //let calendarYear2 = year_token2.parse::<i32>().unwrap();
                    let datetext2 = context_parts_vec[context_parts_date_locations[1].token_index];
                    let j = datetext2.find(&year_token2).unwrap();

                    if datetext2.len() >= (j + 8) {
                        to_date_string = &datetext2[j..j + 8];
                    } else if datetext2.len() >= (j + 6) {
                        to_date_string = &datetext2[j..j + 6];
                    }
                }

                print!("Unknow context date format");

                //to_date_string = context_parts_vec[context_parts_date_locations[1].index];
            }
        }

        if from_date_string.len() > 7 && to_date_string.len() > 7 {
            from_date = NaiveDate::parse_from_str(from_date_string.trim(), "%Y%m%d")
                .clone()
                .unwrap();
            to_date = NaiveDate::parse_from_str(to_date_string.trim(), "%Y%m%d").unwrap();
            days = to_date.sub(from_date).num_days();
            calendarYear = from_date.year();

            if days > 300 {
                is_annual = true;
            } else {
                is_annual = false;
            }
        }

        let val = Some("default".to_string());
        let valbool = true;

        let dateinfo = DateInfo {
            from_date: Some(from_date.to_string()),
            to_date: Some(to_date.to_string()),
            year: Some(calendarYear.to_string()),
            quarter: Some(quarter.to_string()),
            is_annual: Some(is_annual),
            is_annual_revenue: Some(is_annual_revenue),
            days: Some(days.to_string()),
        };

        dateinfo
    }

    pub async fn load_revenue() {
        let url = "mysql://db_user:db_user_pass@localhost:6033/app_db";
        let pool = Pool::new(url).unwrap();
        let mut conn = pool.get_conn().unwrap();

        let val = Some("default".to_string());
        let valbool = true;

        let mut revenues = vec![Revenue {
            cik: val.clone(),
            key: val.clone(),
            from_date: val.clone(),
            to_date: val.clone(),
            value: val.clone(),
            decimals: val.clone(),
            unitref: val.clone(),
            context: val.clone(),
            year: val.clone(),
            days: val.clone(),
            is_annual: Some(valbool),
            is_annual_revenue: Some(valbool),
        }];

        let facts = select_revenue().await;

        for fact in facts.iter() {
            if !fact
                .keytext
                .as_ref()
                .unwrap()
                .to_string()
                .contains("TextBlock")
            {
                let date_info = get_date_info(fact.context.clone()).await;

                let revenue = Revenue {
                    cik: fact.cik.clone(),
                    key: fact.keytext.clone(),
                    from_date: date_info.from_date,
                    to_date: date_info.to_date,
                    value: fact.valuetext.clone(),
                    decimals: fact.decimals.clone(),
                    unitref: fact.unitref.clone(),
                    context: fact.context.clone(),
                    year: date_info.year,
                    days: date_info.days,
                    is_annual: date_info.is_annual,
                    is_annual_revenue: date_info.is_annual_revenue,
                };

                revenues.push(revenue);
                // }
            };
        }

        revenues.remove(0); // remove default record

        conn.exec_batch(
            r"INSERT INTO revenue (cik, keytext, fromdate, todate,  value, decimals, unitref, context, year, days, is_annual, is_annual_revenue)
          VALUES (:cik, :keytext, :fromdate, :todate, :value, :decimals, :unitref, :context, :year, :days, :is_annual, :is_annual_revenue)",
            revenues.iter().map(|r| {
                params! {
                    "cik" => &r.cik,
                    "keytext" => &r.key,
                    "fromdate" => &r.from_date,
                    "todate" => &r.to_date,
                    "value" => &r.value,
                    "decimals" => &r.decimals,
                    "unitref" => &r.unitref,
                    "context" => &r.context,
                    "year" => &r.year,
                    "days" => &r.days,
                    "is_annual" => &r.is_annual,
                    "is_annual_revenue" => &r.is_annual_revenue
                }
            }),
        )
        .unwrap();
    }

    async fn select_income() -> Vec<Fact> {
        let url = "mysql://db_user:db_user_pass@localhost:6033/app_db";
        let pool = Pool::new(url).unwrap();
        let mut conn = pool.get_conn().unwrap();
        // Let's select payments from database. Type inference should do the trick here.
        let facts = conn.query_map(
            "SELECT cik, id, keytext, valuetext, context, decimals, unitref from findata WHERE keytext like '%income%'",
            |(cik, id, keytext, valuetext, context, decimals, unitref)| Fact {
                cik,
                id,
                keytext,
                valuetext,
                context,
                decimals,
                unitref
            },
        ).unwrap();

        facts
    }

    pub async fn load_income() {
        let url = "mysql://db_user:db_user_pass@localhost:6033/app_db";
        let pool = Pool::new(url).unwrap();
        let mut conn = pool.get_conn().unwrap();

        let val = Some("default".to_string());
        let valbool = true;

        let mut revenues = vec![Revenue {
            cik: val.clone(),
            key: val.clone(),
            from_date: val.clone(),
            to_date: val.clone(),
            value: val.clone(),
            decimals: val.clone(),
            unitref: val.clone(),
            context: val.clone(),
            year: val.clone(),
            days: val.clone(),
            is_annual: Some(valbool),
            is_annual_revenue: Some(valbool),
        }];

        let facts = select_income().await;

        for fact in facts.iter() {
            if !fact
                .keytext
                .as_ref()
                .unwrap()
                .to_string()
                .contains("TextBlock")
            {
                // extract year and quarter
                let contextparts = fact.context.as_ref().unwrap().split("_");
                let context_part_count = contextparts.clone().count();
                let context_parts_vec: Vec<&str> = contextparts.collect();
                let mut from_date_string = "";
                let mut to_date_string = "";
                let mut is_annual_revenue = false;

                if context_part_count > 5 {
                    from_date_string = context_parts_vec[4];
                    to_date_string = context_parts_vec[5];
                }

                if context_part_count == 4 {
                    from_date_string = context_parts_vec[2];
                    to_date_string = context_parts_vec[3];
                    is_annual_revenue = true;
                }

                if from_date_string.len() > 7
                    && to_date_string.len() > 7
                    && from_date_string.len() < 9
                    && to_date_string.len() < 9
                {
                    println!("from date:{:?}", from_date_string);
                    println!("to date:{:?}", to_date_string);
                    let from_date =
                        NaiveDate::parse_from_str(from_date_string.trim(), "%Y%m%d").unwrap();
                    let to_date =
                        NaiveDate::parse_from_str(to_date_string.trim(), "%Y%m%d").unwrap();
                    let days = to_date.sub(from_date).num_days();
                    let calendarYear = from_date.year();
                    let mut is_annual = false;

                    if days > 300 {
                        is_annual = true;
                    } else {
                        is_annual = false;
                    }

                    let revenue = Revenue {
                        cik: fact.cik.clone(),
                        key: fact.keytext.clone(),
                        from_date: Some(from_date.to_string()),
                        to_date: Some(to_date.to_string()),
                        value: fact.valuetext.clone(),
                        decimals: fact.decimals.clone(),
                        unitref: fact.unitref.clone(),
                        context: fact.context.clone(),
                        year: Some(calendarYear.to_string()),
                        days: Some(days.to_string()),
                        is_annual: Some(is_annual),
                        is_annual_revenue: Some(is_annual_revenue),
                    };

                    revenues.push(revenue);
                }
            };
        }

        conn.exec_batch(
            r"INSERT INTO revenue (cik, keytext, fromdate, todate,  value, decimals, unitref, context, year, days, is_annual, is_annual_revenue)
          VALUES (:cik, :keytext, :fromdate, :todate, :value, :decimals, :unitref, :context, :year, :days, :is_annual, is_annual_revenue)",
            revenues.iter().map(|r| {
                params! {
                    "cik" => &r.cik,
                    "keytext" => &r.key,
                    "fromdate" => &r.from_date,
                    "todate" => &r.to_date,
                    "value" => &r.value,
                    "decimals" => &r.decimals,
                    "unitref" => &r.unitref,
                    "context" => &r.context,
                    "year" => &r.year,
                    "days" => &r.days,
                    "is_annual" => &r.is_annual,
                    "is_annual_revenue" => &r.is_annual_revenue
                }
            }),
        )
        .unwrap();
    }
}
